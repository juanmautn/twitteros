<?php
require_once 'consulta.php';
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>TP Sistema Operativo</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="css/bootstrap.css" rel="stylesheet">
  <style>
    body {
      padding-top: 60px; 
    }
    #form{
    	float: right;
    	margin-right: 50px;
    }
    input[type="submit"] {
		display: block;
		margin: 0 auto;
	}
	label{
		text-align: center;
	}
  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>
<body>
  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">
        <a class="brand" href="index.php">Aplicacion con la Api de Twitter</a>
      </div>
    </div>
  </div>
  <div id="form">
  	<form action="index.php" method="get">
  		<label>Ingresa una palabra a buscar: </label><input type="text" name="q">
  		<input type="submit" value="buscar">
  	</form>
  </div>
  <div class="container">
    <h1>Resultados de la consulta al Servidor:</h1>
	</br>
    <?php 
    if($results){
      for ($i=0; $i < 5; $i++){    
        echo("</br><p>");
        print_r($results->statuses[$i]->text);
        echo("</p></br>");
        echo("<hr>");
      }
    }  
    ?>
  </div> 
</body>
</html>